package es.jdvf.gestorhogar.calendario.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.jdvf.gestorhogar.calendario.dto.Cita;
import es.jdvf.gestorhogar.calendario.exception.CalendarioException;
import es.jdvf.gestorhogar.calendario.service.CalendarioService;
import io.swagger.annotations.ApiOperation;

/**
 * Controller gestión calendario
 *
 */
@RestController
@RequestMapping("/api/calendario")
public class CalendarioController {
	private static final Logger LOGGER = LoggerFactory.getLogger(CalendarioController.class);

	@Autowired
	private CalendarioService service;

	/**
	 * Devuelve avisos del usuario
	 * 
	 * @param email
	 * @return
	 */
	@ApiOperation(value = "Devuelve avisos del usuario.", response = Cita.class)
	@RequestMapping(value = "/{email}/avisos", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.CREATED)
	ResponseEntity<List<Cita>> avisos(@PathVariable("email") String email) {
		LOGGER.info("avisos - begin - email: {}", email);
		List<Cita> result = service.avisos(email);
		LOGGER.info("avisos - result: {}", result);
		return ResponseEntity.ok(result);
	}

	@ExceptionHandler
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleTodoNotFound(CalendarioException ex) {
		LOGGER.error("Handling error with message: {}", ex.getMessage());
	}
}
