package es.jdvf.gestorhogar.calendario.service;

import java.util.List;

import es.jdvf.gestorhogar.calendario.dto.Cita;

public interface CalendarioService {

	List<Cita> avisos(String email);

}
