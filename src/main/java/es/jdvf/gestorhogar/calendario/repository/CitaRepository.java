package es.jdvf.gestorhogar.calendario.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import es.jdvf.gestorhogar.calendario.entity.CitaEntity;

public interface CitaRepository extends MongoRepository<CitaEntity, String> {

}
