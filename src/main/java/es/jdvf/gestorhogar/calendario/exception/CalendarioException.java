package es.jdvf.gestorhogar.calendario.exception;

public class CalendarioException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CalendarioException(String id) {
        super(String.format("Exception with id: <%s>", id));
    }
}
