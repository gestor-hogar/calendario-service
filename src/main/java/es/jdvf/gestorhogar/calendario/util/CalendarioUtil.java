package es.jdvf.gestorhogar.calendario.util;

import es.jdvf.gestorhogar.calendario.dto.Cita;
import es.jdvf.gestorhogar.calendario.entity.CitaEntity;

public class CalendarioUtil {

	/**
	 * Devuelve objeto DTO a partir de la entidad
	 * @param entity
	 * @return
	 */
	public static Cita convertToDTO(CitaEntity entity) {
		return Cita.builder().id(entity.getId())
				.tipo(entity.getTipo())
				.build();
	}
	
	/**
	 * Devuelve objeto entidad a partir del DTO
	 * @param dto
	 * @return
	 */
	public static CitaEntity convertToEntity(Cita dto) {
		return CitaEntity.builder().id(dto.getId())
				.tipo(dto.getTipo())
				.build();
	}
}
