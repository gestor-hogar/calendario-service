package es.jdvf.gestorhogar.calendario.entity;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Document(collection = "cita")
public class CitaEntity {
	// id
	private String id;
	// tipo
	private String tipo;
	// descripcion
	private String descripcion;
	// asignado
	private String asignado;
	// fecha
	private String fecha;
}